package com.control.back.halo.manage.service.impl;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.control.back.halo.basic.dao.IBaseDao;
import com.control.back.halo.basic.entity.vo.TreeNode;
import com.control.back.halo.basic.log4j.Logger;
import com.control.back.halo.basic.service.impl.BaseServiceImpl;
import com.control.back.halo.manage.dao.IRoleDao;
import com.control.back.halo.manage.entity.Function;
import com.control.back.halo.manage.entity.Role;
import com.control.back.halo.manage.service.IFunctionService;
import com.control.back.halo.manage.service.IRoleService;

@Service
public class RoleServiceImpl extends BaseServiceImpl<Role, Long> implements IRoleService {

    private static Logger    logger = Logger.getLogger(RoleServiceImpl.class);

    @Autowired
    private IRoleDao         roleDao;

    @Autowired
    private IFunctionService functionService;

    @Override
    public IBaseDao<Role, Long> getBaseDao() {
        return this.roleDao;
    }

    @Override
    public Role saveOrUpdate(Role role) {
        if (role.getId() != null) {
            Role roleOne = find(role.getId());
            roleOne.setName(role.getName());
            return update(roleOne);
        } else {
            return save(role);
        }
    }

    @Override
    public List<TreeNode> loadTreeAndMarkRoleFunctions(Long roleId) {
        Role role = find(roleId);
        Set<Function> roleFunctionSet = role.getFunctions();
        List<Function> functionArray = functionService.findAll();
        List<TreeNode> tnArray = functionArray.stream().map(f -> {
            TreeNode node = new TreeNode();
            node.setId(f.getId());
            if (f.getParent() == null) {
                node.setPid(-1L);
            } else {
                node.setPid(f.getParent().getId());
            }
            node.setName(f.getName());
            if (f.getChild() != null) {
                node.setParent(true);
            }

            if (roleFunctionSet.contains(f)) {
                node.setChecked(true);
            }
            return node;
        }).collect(toList());
        return tnArray;
    }

    @Override
    public Boolean saveRoleFunctions(Long roleId, Set<Function> functions) {
        try {
            Role role = find(roleId);
            Set<Function> newFunctions = functions.stream().map(f -> functionService.find(f.getId())).collect(toSet());
            role.setFunctions(newFunctions);
            super.save(role);
        } catch (Exception e) {
            logger.error("Exception params:[roleId:%s,nodes:%s]", e, roleId.toString(), JSON.toJSONString(functions));
            return false;
        }
        return true;
    }
}
