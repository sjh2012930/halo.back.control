package com.control.back.halo.manage.service.impl;

import static java.util.stream.Collectors.toSet;

import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.control.back.halo.basic.dao.IBaseDao;
import com.control.back.halo.basic.entity.User;
import com.control.back.halo.basic.log4j.Logger;
import com.control.back.halo.basic.service.impl.BaseServiceImpl;
import com.control.back.halo.manage.dao.IAdminDao;
import com.control.back.halo.manage.entity.Admin;
import com.control.back.halo.manage.entity.Member;
import com.control.back.halo.manage.entity.Role;
import com.control.back.halo.manage.service.IAdminService;
import com.control.back.halo.manage.service.IRoleService;

@Service
public class AdminServiceImpl extends BaseServiceImpl<Admin, Long> implements IAdminService {

    private static Logger logger = Logger.getLogger(AdminServiceImpl.class);

    @Autowired
    private IAdminDao     adminDao;

    @Autowired
    private IRoleService  roleService;

    @Value("${halo.frame.variables.default-role}")
    private Long          defaultUserRoleId;

    @Override
    public IBaseDao<Admin, Long> getBaseDao() {
        return this.adminDao;
    }

    @Override
    public Admin findByUsername(String username) {
        return adminDao.findByUsername(username);
    }

    @Override
    public Admin saveOrUpdate(Admin admin) {
        Assert.notNull(admin, "admin 不能为空!");

        if (admin.getId() != null) {
            Admin adminOne = find(admin.getId());
            User user = adminOne.getUser();
            if (admin.getUser() != null) {
                user.setUsername(admin.getUser().getUsername());
                if (StringUtils.isNotBlank(admin.getUser().getPassword())) {
                    String password = new Sha256Hash(admin.getUser().getPassword()).toHex();
                    user.setPassword(password);
                }
            }
            Member member = adminOne.getMember();
            if (member == null) {
                adminOne.setMember(admin.getMember());
            } else {
                if (admin.getMember() != null) {
                    member.setBirthday(admin.getMember().getBirthday());
                    member.setSex(admin.getMember().getSex());
                    member.setName(admin.getMember().getName());
                }
            }
            return update(adminOne);
        } else {
            if (admin.getUser() != null) {
                if (StringUtils.isNotBlank(admin.getUser().getPassword())) {
                    String password = new Sha256Hash(admin.getUser().getPassword()).toHex();
                    admin.getUser().setPassword(password);
                }
            }
            Role role = roleService.find(defaultUserRoleId);
            admin.addToRole(role);
            return save(admin);
        }
    }

    @Override
    public Boolean saveAdminRoles(Long id, Long[] roles) {
        try {
            Admin admin = find(id);
            Set<Role> rs =  Stream.of(roles).map(roleId -> roleService.find(roleId))
                    .collect(toSet());
            admin.setRoles(rs);
            super.save(admin);
        } catch (Exception e) {
            logger.error("Exception params:[id:%s,roles:%s]", e, id.toString(), roles);
            return false;
        }
        return true;
    }
}
