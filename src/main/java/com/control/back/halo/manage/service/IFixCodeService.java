package com.control.back.halo.manage.service;

import java.util.List;

import com.control.back.halo.basic.service.IBaseService;
import com.control.back.halo.manage.entity.FixCode;

public interface IFixCodeService extends IBaseService<FixCode, Long> {

    /**
     * 
     * 功能描述: <br>
     * 根据代码类型获取数据字典列表信息
     *
     * @param codeType
     *            代码类型
     * @return 数据字典列表信息
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    List<FixCode> getFixCodeByCodeType(Integer codeType);

    /**
     * 
     * 功能描述: <br>
     * 根据数据字典代码获取数据字典信息
     *
     * @param code
     *            代码
     * @return 数据字典信息
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    FixCode getFixCodeByCode(Integer code);

    FixCode getFixCodeByCodeAndType(Integer codeType, Integer code);

}
