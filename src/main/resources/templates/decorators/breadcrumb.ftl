<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>${(function.name)!}</h2>
        <ol class="breadcrumb">
            <li><a href="index.htm">主页</a></li>
            <li><a>${(function.parent.name)!}</a></li>
            <li><a>${(function.name)!}</a></li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>